<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Users::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'role' => $faker->randomElement(['user'])
    ];
});

$factory->define(App\Models\Products::class, function (Faker\Generator $faker) {
    
    return [
        'name' => $faker->shuffleString('ABCDEFGH'),
        'points' => $faker->randomElement([100,200,500,600,800])
    ];
});

$factory->define(App\Models\Coupons::class, function (Faker\Generator $faker) {
    
    return [
        'name' => $faker->shuffleString('ABCD12'),
        'points' => $faker->randomElement([100,150,250,300])
    ];
});
