<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();

    	factory(App\Models\Users::class)->create([
        	'name' =>'Admin User',
        	'email' => 'admin@admin.com',
        	'password' => bcrypt('admin'),
        	'role' => 'admin'
        	]);
        factory(App\Models\Users::class, 40)->create();
    }
}
