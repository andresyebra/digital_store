<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
  <link rel='stylesheet prefetch' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css'>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
</head>
        <!-- Styles -->
<style type="text/css">
.tab-container {
  background: #fff;
  border: 1px solid #ccc;
}
.tab-container .nav-tabs {
  border-bottom: 1px solid #ccc;
  position: relative;
}
.tab-container .nav-tabs > li {
  margin-bottom: 0;
  position: initial;
}
.tab-container .nav-tabs > li > a {
  border: none;
  color: #999;
  margin: 0;
}
.tab-container .nav-tabs > li > a:hover {
  background: transparent;
}
.tab-container .nav-tabs > li.active > a {
  border: none;
  color: #E91E63;
  background: transparent;
}
.tab-container .nav-tabs > li.active > a,
.tab-container .nav-tabs > li.active > a:focus,
.tab-container .nav-tabs > li.active > a:hover {
  border-width: 0;
}
.tab-container .nav-tabs > li#magic-line {
  position: absolute;
  bottom: -1px;
  left: 0;
  width: 100px;
  height: 2px;
  background: #E91E63;
}
.tab-container .tab-content {
  padding: 15px;
}

</style>
<div class="container">
  <div class="row">
    <div class="col-md-8 col-md-offset-2">
      <div class="tab-container">
        <ul class="nav nav-tabs" role="tablist" id="example-one">
          <li role="presentation">
            <a href="#first" aria-controls="first" role="tab" data-toggle="tab">First</a>
          </li>
          <li role="presentation" class="active">
            <a href="#second" aria-controls="second" role="tab" data-toggle="tab">Second</a>
          </li>
          <li role="presentation">
            <a href="#third" aria-controls="third" role="tab" data-toggle="tab">Third</a>
          </li>
          <li role="presentation">
            <a href="#fourth" aria-controls="fourth" role="tab" data-toggle="tab">Fourth</a>
          </li>
        </ul>
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane" id="first">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec convallis dui ligula, pretium dignissim erat semper non. Cras ultricies faucibus libero, id blandit nisi volutpat nec. Mauris sit amet ultrices ex. Donec bibendum dui id libero molestie efficitur.
            Ut egestas porta ligula, eget iaculis est finibus sed. Integer consectetur nec lorem vitae malesuada. Maecenas quis porta orci. Cras eleifend dui quis justo bibendum, nec pharetra risus blandit. Sed pharetra magna orci, eget eleifend sapien
            pretium in. Nulla cursus iaculis turpis vitae ullamcorper. Vivamus fringilla mattis gravida. Vestibulum sit amet porta justo.
            <br>
            <br> Vestibulum fringilla risus sed ante porta, sit amet eleifend tortor scelerisque. Maecenas euismod dignissim molestie. Sed est massa, laoreet et velit nec, porta dapibus massa. Aenean id commodo lectus. Aenean et dictum purus, sed hendrerit
            sapien. Maecenas ipsum ligula, aliquet sit amet felis eget, tincidunt pretium erat. In posuere augue a sem convallis suscipit ut a enim.
          </div>
          <div role="tabpanel" class="tab-pane active" id="second">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec convallis dui ligula, pretium dignissim erat semper non. Cras ultricies faucibus libero, id blandit nisi volutpat nec. Mauris sit amet ultrices ex. Donec bibendum dui id libero molestie efficitur.
            Ut egestas porta ligula, eget iaculis est finibus sed. Integer consectetur nec lorem vitae malesuada. Maecenas quis porta orci. Cras eleifend dui quis justo bibendum, nec pharetra risus blandit. Sed pharetra magna orci, eget eleifend sapien
            pretium in. Nulla cursus iaculis turpis vitae ullamcorper. Vivamus fringilla mattis gravida. Vestibulum sit amet porta justo.
          </div>
          <div role="tabpanel" class="tab-pane" id="third">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec convallis dui ligula, pretium dignissim erat semper non. Cras ultricies faucibus libero, id blandit nisi volutpat nec. Mauris sit amet ultrices ex. Donec bibendum dui id libero molestie efficitur.
            Ut egestas porta ligula, eget iaculis est finibus sed. Integer consectetur nec lorem vitae malesuada. Maecenas quis porta orci. Cras eleifend dui quis justo bibendum, nec pharetra risus blandit. Sed pharetra magna orci, eget eleifend sapien
            pretium in. Nulla cursus iaculis turpis vitae ullamcorper. Vivamus fringilla mattis gravida. Vestibulum sit amet porta justo.
            <br>
            <br> Vestibulum fringilla risus sed ante porta, sit amet eleifend tortor scelerisque. Maecenas euismod dignissim molestie. Sed est massa, laoreet et velit nec, porta dapibus massa. Aenean id commodo lectus. Aenean et dictum purus, sed hendrerit
            sapien. Maecenas ipsum ligula, aliquet sit amet felis eget, tincidunt pretium erat. In posuere augue a sem convallis suscipit ut a enim.
          </div>
          <div role="tabpanel" class="tab-pane" id="fourth">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec convallis dui ligula, pretium dignissim erat semper non. Cras ultricies faucibus libero, id blandit nisi volutpat nec. Mauris sit amet ultrices ex. Donec bibendum dui id libero molestie efficitur.
            Ut egestas porta ligula, eget iaculis est finibus sed. Integer consectetur nec lorem vitae malesuada. Maecenas quis porta orci. Cras eleifend dui quis justo bibendum, nec pharetra risus blandit. Sed pharetra magna orci, eget eleifend sapien
            pretium in. Nulla cursus iaculis turpis vitae ullamcorper. Vivamus fringilla mattis gravida. Vestibulum sit amet porta justo.
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</html>
