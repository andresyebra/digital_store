@extends('layout')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4>Coupon Information</h4></div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ url('coupon/new')}}">
                            {!! csrf_field() !!}
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="id_coupon">Coupon ID</label>
                                    <div class="col-md-4">
                                        <input id="id_coupon" name="id_coupon" type="text" placeholder="ID"
                                               class="form-control input-md">

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="name_coupon">Coupon Name</label>
                                    <div class="col-md-4">
                                        <input id="name_coupon" name="name_coupon" type="text" placeholder="Name"
                                               class="form-control input-md">

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="points_coupon">Points</label>
                                    <div class="col-md-4">
                                        <input id="points_coupon" name="points_coupon" type="text" placeholder="Points"
                                               class="form-control input-md">

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="delete_coupon"></label>
                                    <div class="col-md-4">
                                        <button id="add_coupon" type="submit" name="add_coupon" class="btn btn-primary"
                                                value="add">Add
                                        </button>
                                        <button id="delete_coupon" type="submit" name="delete_coupon"
                                                class="btn btn-primary" value="delete">Delete
                                        </button>
                                    </div>
                                </div>

                                @if(!empty($_GET['message']))
                                    <div class="col-md-4 col-md-offset-2">
                                        <div class="alert alert-success" role="alert">{{ $_GET['message'] }}</div>
                                    </div>
                                @endif
                                @if($errors->any())
                                    <div class="col-md-4 col-md-offset-2">
                                        <div class="alert alert-danger" role="alert">
                                            {{$errors->first()}}
                                        </div>
                                    </div>
                                @endif

                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4>Coupon List</h4></div>
                    <div class="panel-body">

                        <table class="table">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">Coupon Id</th>
                                <th scope="col">Coupon Name</th>
                                <th scope="col">Points</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($coupons) > 0)
                                @foreach ($coupons as $coupon => $value)
                                    <tr>
                                        <td>{{$value->id}}</td>
                                        <td>{{$value->name}}</td>
                                        <td>{{$value->points}}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td>don't exists coupons</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
        </div>
    </div>









@endsection