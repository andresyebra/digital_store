@extends('layout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4>Product Information</h4></div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ url('product/new')}}">
                            {!! csrf_field() !!}
                            <fieldset>

                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="id_product">Product ID</label>
                                    <div class="col-md-4">
                                        <input id="id_product" name="id_product" type="text" placeholder="ID"
                                               class="form-control input-md">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="name_product">Product Name</label>
                                    <div class="col-md-4">
                                        <input id="name_product" name="name_product" type="text" placeholder="Name"
                                               class="form-control input-md">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="points_product">Points</label>
                                    <div class="col-md-4">
                                        <input id="points_product" name="points_product" type="text"
                                               placeholder="Points" class="form-control input-md">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 control-label" for="delete_product"></label>
                                    <div class="col-md-4">
                                        <button id="add_product" type="submit" name="add_product"
                                                class="btn btn-primary" value="add">Add
                                        </button>
                                        <button id="delete_product" type="submit" name="delete_product"
                                                class="btn btn-primary" value="delete">Delete
                                        </button>
                                    </div>
                                </div>

                                @if(!empty($_GET['message']))
                                    <div class="col-md-4 col-md-offset-2">
                                        <div class="alert alert-success" role="alert">{{ $_GET['message'] }}</div>
                                    </div>
                                @endif

                                @if($errors->any())
                                    <div class="col-md-4 col-md-offset-2">
                                        <div class="alert alert-danger" role="alert">
                                            {{$errors->first()}}
                                        </div>
                                    </div>
                                @endif

                            </fieldset>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4>Product List</h4></div>
                    <div class="panel-body">

                        <table class="table">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">Product Id</th>
                                <th scope="col">Product Name</th>
                                <th scope="col">Points</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($products) > 0)
                                @foreach ($products as $product => $value)
                                    <tr>
                                        <td>{{$value->id}}</td>
                                        <td>{{$value->name}}</td>
                                        <td>{{$value->points}}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr>
                                    <td>don't exists products</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection 


