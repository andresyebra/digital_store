@extends('layout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4>My information</h4></div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4"><b>Name:</b> {{$profile->name}}</div>
                            <div class="col-md-4 offset-md-4"><b>Total Product Points:</b> {{ $total_products}}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-4"><b>Email:</b> {{$profile->email}}</div>
                            <div class="col-md-4 offset-md-4"><b>Total Coupon Points:</b> {{ $total_coupons}}</div>
                        </div>
                        <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4 offset-md-4"><b>Aviable Points:</b> {{$aviable_points}}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4>My Products</h4></div>
                    <div class="panel-body">

                        <table class="table">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">Product Id</th>
                                <th scope="col">Product Name</th>
                                <th scope="col">Points</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($products) > 0 )
                                @foreach ($products as $product => $value)
                                    <tr>
                                        <td>{{$value->product_id}}</td>
                                        <td>{{$value->name}}</td>
                                        <td>{{$value->points}}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td></td>
                                    <td><b>Total</b></td>
                                    <td><b>{{ $total_products}}</b></td>
                                </tr>
                            @else
                                <tr>
                                    <td>User don't have products</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4>My Coupons</h4></div>
                    <div class="panel-body">

                        <table class="table">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">Coupon Id</th>
                                <th scope="col">Coupon Name</th>
                                <th scope="col">Points</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($coupons) > 0)
                                @foreach ($coupons as $coupon => $value)
                                    <tr>
                                        <td>{{$value->coupon_id}}</td>
                                        <td>{{$value->name}}</td>
                                        <td>{{$value->points}}</td>
                                    </tr>
                                @endforeach
                                <tr>
                                    <td></td>
                                    <td><b>Total</b></td>
                                    <td><b>{{ $total_coupons }}</b></td>
                                </tr>
                            @else
                                <tr>
                                    <td>User don't have coupons</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

