@extends('layout')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading"><h4>Users Ranking</h4></div>
                    <div class="panel-body">
                        <table class="table">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">User Id</th>
                                <th scope="col">Name</th>
                                <th scope="col">Score</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(count($users) > 0)
                                @if(Auth::user()->role == 'admin')
                                    @foreach ($users as $user)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$user->id}}</td>
                                            <td><a href="{{$user->id}}">{{ $user->name }}</a></td>
                                            <td>{{$user->coupons_points}}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    @foreach ($users as $user)
                                        <tr>
                                            <td>{{$loop->iteration}}</td>
                                            <td>{{$user->id}}</td>
                                            <td>{{ $user->name }}</td>
                                            <td>{{$user->coupons_points}}</td>
                                        </tr>
                                    @endforeach
                                @endif
                            @else
                                <tr>
                                    <td>Without users</td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection