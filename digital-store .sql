-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-04-2018 a las 11:07:17
-- Versión del servidor: 10.1.31-MariaDB
-- Versión de PHP: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `digital-store`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coupons`
--

CREATE TABLE `coupons` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `points` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `coupons`
--

INSERT INTO `coupons` (`id`, `name`, `points`) VALUES
(1, '2B1ADC', 250),
(2, '1CAB2D', 150),
(3, '21BACD', 250),
(4, 'BA1D2C', 150),
(5, '1DAC2B', 250),
(6, 'CAB12D', 100),
(7, 'BD12CA', 150),
(8, '2A1BDC', 100),
(9, '21BDCA', 300),
(10, 'DCB1A2', 150),
(11, 'D12ACB', 250),
(12, 'BCD2A1', 250),
(13, 'BDA2C1', 150),
(14, 'BC2D1A', 150),
(15, 'A21CDB', 150),
(16, 'A12BDC', 250),
(17, 'AB2CD1', 150),
(18, '21DABC', 150),
(19, 'BA21CD', 300),
(20, 'BA12CD', 100),
(21, 'C1BAD2', 150),
(22, 'A1BC2D', 250),
(23, 'DB12CA', 300),
(24, 'BDA1C2', 250),
(25, '2DB1CA', 250),
(26, 'D12BAC', 100),
(27, 'D2BCA1', 150),
(28, '2CDBA1', 100),
(29, 'BD2C1A', 250),
(30, 'DCA1B2', 300),
(31, 'AD2C1B', 300),
(32, 'A1D2BC', 250),
(33, 'ACDB12', 300),
(34, '21DCAB', 150),
(35, 'D1CAB2', 250),
(36, 'CAB12D', 100),
(37, '1BDA2C', 250),
(38, '2CDA1B', 100),
(39, '12DCAB', 100),
(40, 'ACD12B', 150),
(41, 'ACB1D2', 250),
(42, 'CA2BD1', 250),
(43, 'ACB1D2', 150),
(45, 'B1ADC2', 150),
(46, 'B12DAC', 150),
(47, 'CB21DA', 100),
(48, '1B2ADC', 100),
(49, '2B1ACD', 250),
(50, 'C2D1BA', 300),
(51, '2DBCA1', 100),
(52, 'CDB12A', 100),
(53, 'BAC2D1', 100),
(54, 'B1DC2A', 100),
(55, 'B2DCA1', 300),
(56, 'DCA12B', 250),
(57, 'AD12CB', 300),
(58, '2ADC1B', 250),
(59, '1CA2DB', 250),
(60, 'CA2D1B', 300),
(61, 'AC21BD', 100),
(62, 'ADC2B1', 150),
(63, 'A2BDC1', 100),
(64, 'B21ACD', 100),
(65, '2AB1DC', 150),
(66, 'DA1CB2', 300),
(67, 'A21BCD', 300),
(68, 'ACB12D', 250),
(69, 'CB1A2D', 150),
(70, 'D2A1CB', 250),
(71, '2D1CAB', 150),
(72, '1CDAB2', 150),
(73, 'DABC21', 250),
(74, '2BC1DA', 250),
(75, '12CDBA', 100),
(76, '1CBA2D', 100),
(78, 'A21BCD', 300),
(79, 'C1BDA2', 250),
(80, 'DAB1C2', 100),
(81, '2CDB1A', 150),
(82, 'BADC12', 300),
(83, 'A2BDC1', 150),
(84, 'BCD2A1', 150),
(85, 'C21BDA', 250),
(86, 'C1BD2A', 300),
(87, 'A1DCB2', 150),
(88, '1DBC2A', 250),
(89, '2C1BAD', 150),
(90, 'C1ABD2', 300),
(91, '12CBAD', 300),
(92, '2AC1DB', 300),
(93, '1B2CAD', 100),
(94, 'B2CAD1', 150),
(95, 'C21DAB', 150),
(96, '12CBDA', 300),
(97, 'DBAC12', 150),
(98, '12CDBA', 150),
(99, '2CA1DB', 250),
(102, 'erdccv', 342);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(5, '2014_10_12_000000_create_users_table', 1),
(6, '2014_10_12_100000_create_password_resets_table', 1),
(7, '2018_03_30_073518_create_products_table', 1),
(8, '2018_03_30_082400_create_coupons_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `points` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `products`
--

INSERT INTO `products` (`id`, `name`, `points`) VALUES
(1, 'BGCHEFDA', 150),
(3, 'FECGDAHB', 100),
(5, 'EDBHAFCG', 100),
(6, 'BEACDGHF', 120),
(8, 'EFHCDGAB', 200),
(9, 'GECHDFAB', 120),
(10, 'BGDFHACE', 120),
(11, 'HFEACDGB', 100),
(12, 'GHBEDFAC', 200),
(13, 'ADFCEGHB', 150),
(14, 'DAFHCGBE', 100),
(15, 'DCFBGHEA', 150),
(16, 'AFDCHEGB', 150),
(17, 'EHGFDBCA', 120),
(18, 'GHEBFDAC', 200),
(19, 'GACHBEDF', 100),
(20, 'HDGFABEC', 100),
(21, 'FCEHDBAG', 120),
(22, 'DHCEABGF', 100),
(23, 'CHDEABFG', 100),
(24, 'AHBGFEDC', 100),
(25, 'FBADHGEC', 150),
(26, 'EFHDCGAB', 150),
(27, 'GEFCDAHB', 200),
(28, 'CGBDAFHE', 200),
(29, 'BDEFGACH', 200),
(30, 'GFABCHED', 200),
(31, 'EDFHGBAC', 200),
(32, 'CABDFEGH', 100),
(33, 'BEFHGDAC', 120),
(34, 'GCFDBAHE', 200),
(35, 'EBFDGAHC', 200),
(36, 'HEBCFAGD', 150),
(37, 'GAFCDEBH', 100),
(38, 'EGBCDHFA', 150),
(39, 'DGABFECH', 100),
(40, 'FABEHCGD', 120),
(41, 'CHGBFAED', 200),
(42, 'GHEABCDF', 120),
(43, 'FABEGHDC', 120),
(44, 'DEBCFAHG', 120),
(45, 'GDEACBHF', 200),
(46, 'DGFAHECB', 150),
(47, 'GDCEAHBF', 100),
(49, 'BEFCGDAH', 120);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role` enum('user','admin') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `role`) VALUES
(1, 'Admin User', 'admin@admin.com', '$2y$10$31R.3t1FXbBd8SK1Iua6keQlvi4UfcSNXwFMT2iow65Ly19hCmxLq', 'hTOxF04t9ONJRT8g9mU1YxaDb1Y8qShF9k1PXj4V807zooTQihssV9lblMNt', '2018-03-31 06:09:32', '2018-03-31 06:09:32', 'admin'),
(2, 'Alexie Aufderhar', 'thiel.tressa@example.org', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:32', '2018-03-31 06:09:32', 'user'),
(3, 'Dr. Piper Parker V', 'bdamore@example.net', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', 'wki7u6Lg8njMtjuqiQ21GdNUy3ecr31SBLVJCxtDLPzGd5CPHR2JVNfp6iit', '2018-03-31 06:09:32', '2018-03-31 06:09:32', 'user'),
(4, 'Orlando Mills', 'ijenkins@example.com', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:32', '2018-03-31 06:09:32', 'user'),
(5, 'Miss Myrtis Connelly', 'kevin.simonis@example.net', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:32', '2018-03-31 06:09:32', 'user'),
(6, 'Mr. Kiel Heidenreich PhD', 'dbartoletti@example.com', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:32', '2018-03-31 06:09:32', 'user'),
(7, 'Reinhold Bernier', 'susan.lindgren@example.net', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:32', '2018-03-31 06:09:32', 'user'),
(8, 'Opal Larkin I', 'pacocha.ryder@example.com', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:32', '2018-03-31 06:09:32', 'user'),
(9, 'Johnpaul Greenfelder III', 'runte.trenton@example.net', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:33', '2018-03-31 06:09:33', 'user'),
(10, 'Mrs. Kenyatta Christiansen PhD', 'crona.dorcas@example.org', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:33', '2018-03-31 06:09:33', 'user'),
(11, 'Emma Satterfield IV', 'heaney.adolph@example.org', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:33', '2018-03-31 06:09:33', 'user'),
(12, 'Lesley Ferry', 'schmeler.jaylen@example.org', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:33', '2018-03-31 06:09:33', 'user'),
(13, 'Frida Hansen', 'eschinner@example.com', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:33', '2018-03-31 06:09:33', 'user'),
(14, 'Brenna O\'Conner', 'plarson@example.com', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:33', '2018-03-31 06:09:33', 'user'),
(15, 'Dr. Anderson Greenfelder Jr.', 'mbechtelar@example.org', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:33', '2018-03-31 06:09:33', 'user'),
(16, 'Eleonore Blick', 'ifriesen@example.org', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:33', '2018-03-31 06:09:33', 'user'),
(17, 'Dr. Johan Crooks', 'kim62@example.com', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:33', '2018-03-31 06:09:33', 'user'),
(18, 'Madisyn Dietrich I', 'pouros.mona@example.net', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:33', '2018-03-31 06:09:33', 'user'),
(19, 'Wyman Gerlach MD', 'mac.hauck@example.org', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:33', '2018-03-31 06:09:33', 'user'),
(20, 'Sydni Zboncak', 'nathanael.weimann@example.net', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:33', '2018-03-31 06:09:33', 'user'),
(21, 'Amari Wolf', 'jerald.veum@example.net', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:33', '2018-03-31 06:09:33', 'user'),
(22, 'Barry Blanda', 'robert89@example.net', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:33', '2018-03-31 06:09:33', 'user'),
(23, 'Keenan Feeney', 'janelle.mcdermott@example.com', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:33', '2018-03-31 06:09:33', 'user'),
(24, 'Miss Emelia Kautzer', 'schuster.jarred@example.com', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:33', '2018-03-31 06:09:33', 'user'),
(25, 'Luella Gerlach II', 'dsenger@example.net', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:33', '2018-03-31 06:09:33', 'user'),
(26, 'Abby Schulist', 'jschowalter@example.org', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:34', '2018-03-31 06:09:34', 'user'),
(27, 'Freeda Fadel', 'qthompson@example.com', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:34', '2018-03-31 06:09:34', 'user'),
(28, 'Abby Gerlach I', 'wrutherford@example.org', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:34', '2018-03-31 06:09:34', 'user'),
(29, 'Rhoda Jenkins', 'dhauck@example.org', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:34', '2018-03-31 06:09:34', 'user'),
(30, 'Mrs. Valerie Schulist I', 'elsie.thiel@example.org', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:34', '2018-03-31 06:09:34', 'user'),
(31, 'Gregg Glover', 'jay.conn@example.com', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:34', '2018-03-31 06:09:34', 'user'),
(32, 'Eduardo Bogan', 'elise.dubuque@example.com', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:34', '2018-03-31 06:09:34', 'user'),
(33, 'Maci Stroman', 'price.kayley@example.org', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:34', '2018-03-31 06:09:34', 'user'),
(34, 'Danny Nikolaus', 'nleffler@example.com', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:34', '2018-03-31 06:09:34', 'user'),
(35, 'Ernest Dicki I', 'metz.sallie@example.org', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:35', '2018-03-31 06:09:35', 'user'),
(36, 'Lorna Bashirian', 'dalton82@example.com', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:35', '2018-03-31 06:09:35', 'user'),
(37, 'Trey Reichel', 'charles.hand@example.org', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:35', '2018-03-31 06:09:35', 'user'),
(38, 'Tyrese Walsh', 'otis.borer@example.com', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:35', '2018-03-31 06:09:35', 'user'),
(39, 'Trisha Dietrich', 'wdoyle@example.com', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:35', '2018-03-31 06:09:35', 'user'),
(40, 'Willard Russel Sr.', 'yschultz@example.net', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:35', '2018-03-31 06:09:35', 'user'),
(41, 'Frieda Sporer', 'kendra26@example.net', '$2y$10$3EKMTir3W3rKe8DGPIQuFejip7goki5kslKjgl1LbZHQBW0wfkOkO', NULL, '2018-03-31 06:09:35', '2018-03-31 06:09:35', 'user'),
(42, 'Andres Yebra C', 'andres@yebra.com', '$2y$10$7Olxam1IbJZKW/eWqalFDukcmdMkNTYnar1AFoMVnE1338OAwS2su', 'mTUZNHWApCyp06rXXSkB84HES7PP8RIQxGV8FGgVZshqagFygzuiUB7z7YvD', '2018-04-01 06:31:16', '2018-04-01 06:31:16', 'user');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_coupons`
--

CREATE TABLE `users_coupons` (
  `id` int(11) NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `coupon_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users_coupons`
--

INSERT INTO `users_coupons` (`id`, `user_id`, `coupon_id`) VALUES
(136, 2, 2),
(137, 2, 2),
(333, 15, 65),
(374, 15, 65),
(375, 5, 31),
(376, 42, 21),
(377, 15, 13),
(378, 24, 14),
(379, 21, 2),
(380, 32, 7),
(381, 25, 9),
(382, 40, 22),
(383, 7, 38),
(384, 26, 27),
(385, 22, 7),
(386, 35, 6),
(387, 8, 49),
(388, 9, 41),
(389, 9, 40),
(390, 33, 15),
(391, 22, 8),
(392, 42, 18),
(393, 33, 12),
(394, 38, 37),
(395, 29, 45),
(396, 28, 5),
(397, 27, 11),
(398, 23, 22),
(399, 15, 33),
(400, 12, 1),
(401, 22, 29),
(402, 12, 33),
(403, 13, 49),
(404, 15, 13),
(405, 18, 12),
(406, 18, 39),
(407, 19, 41),
(409, 21, 14);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_products`
--

CREATE TABLE `users_products` (
  `id` int(11) NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `product_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `users_products`
--

INSERT INTO `users_products` (`id`, `user_id`, `product_id`) VALUES
(1, 2, 3),
(2, 2, 1),
(3, 2, 1),
(4, 2, 1),
(5, 2, 3),
(6, 5, 5),
(7, 42, 5),
(8, 15, 11),
(9, 24, 14),
(10, 21, 20),
(11, 32, 20),
(12, 25, 19),
(13, 40, 22),
(14, 7, 23),
(15, 26, 24),
(16, 35, 6),
(17, 8, 49),
(18, 9, 41),
(19, 9, 40),
(20, 33, 15),
(21, 22, 8),
(22, 42, 18),
(23, 33, 12),
(24, 38, 37),
(25, 29, 45),
(26, 28, 5),
(27, 27, 11),
(28, 23, 22),
(29, 15, 33),
(30, 12, 1),
(31, 22, 29),
(32, 12, 33),
(33, 13, 49),
(34, 15, 13),
(35, 18, 12),
(36, 18, 39),
(37, 19, 41),
(38, 10, 30),
(39, 21, 14);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indices de la tabla `users_coupons`
--
ALTER TABLE `users_coupons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `coupon_id` (`coupon_id`);

--
-- Indices de la tabla `users_products`
--
ALTER TABLE `users_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `product_id` (`product_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT de la tabla `users_coupons`
--
ALTER TABLE `users_coupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=410;

--
-- AUTO_INCREMENT de la tabla `users_products`
--
ALTER TABLE `users_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `users_coupons`
--
ALTER TABLE `users_coupons`
  ADD CONSTRAINT `users_coupons_ibfk_1` FOREIGN KEY (`coupon_id`) REFERENCES `coupons` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `users_coupons_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `users_products`
--
ALTER TABLE `users_products`
  ADD CONSTRAINT `users_products_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_products_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
