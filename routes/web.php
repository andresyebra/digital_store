<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index')->name('home');

Route::get('/user/index', 'UserController@index');
Route::get('/user/{id}', 'UserController@profile')->where('id', '[0-9]+');

Route::get('/product/index', 'ProductController@index')->name('product');
Route::post('/product/new/', 'ProductController@create');

Route::get('/coupon/index', 'CouponController@index')->name('coupon');
Route::post('/coupon/new/', 'CouponController@create');

Route::get('/about', function () {
    return view('about.index');
});


Auth::routes();

