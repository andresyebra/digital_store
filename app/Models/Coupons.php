<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Coupons extends Model
{
    public $timestamps = false;

    public static function getCoupons()
    {
        $coupons = DB::table('coupons')->get();

        return $coupons;
    }

    public static function createCoupon($data)
    {

        $idcoupon = DB::table('coupons')->insertGetId([
            'name' => $data['name'],
            'points' => $data['points']
        ]);

        return $idcoupon;
    }

    public static function deleteCoupon($id)
    {
        $delete = DB::table('coupons')->where('id', '=', $id['id'])->delete();
    }
}
