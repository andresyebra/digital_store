<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Users extends Authenticatable
{
    protected $fillable = [
        'name', 'email', 'password',
    ];


    public static function getUsers()
    {

        $users = DB::table('users')
            ->join('users_coupons', 'users_coupons.user_id', '=', 'users.id')
            ->join('coupons', 'coupons.id', '=', 'users_coupons.coupon_id')
            ->select('users.id', 'users.name', DB::raw('sum(coupons.points) as coupons_points'))
            ->groupBy('users.id')
            ->orderBy('coupons_points', 'acs')
            ->get();

        return $users;
    }

    public static function profileUser($id)
    {

        $user = DB::table('users')
            ->where('users.id', '=', $id)
            ->first();

        return $user;
    }

    public static function productUser($id)
    {

        $products = DB::table('users')
            ->join('users_products', 'users.id', '=', 'users_products.user_id')
            ->join('products', 'products.id', '=', 'users_products.product_id')
            ->where('users.id', '=', $id)
            ->select('users_products.product_id', 'products.name', 'products.points')
            ->get();

        return $products;

    }

    public static function couponUser($id)
    {

        $coupon = DB::table('users')
            ->join('users_coupons', 'users.id', '=', 'users_coupons.user_id')
            ->join('coupons', 'coupons.id', '=', 'users_coupons.coupon_id')
            ->where('users.id', '=', $id)
            ->select('coupons.name', 'coupons.points', 'users_coupons.coupon_id')
            ->get();

        return $coupon;

    }


}
