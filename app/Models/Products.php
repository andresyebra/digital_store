<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{

    public $timestamps = false;

    public static function getProducts()
    {
        $products = DB::table('products')->get();
        return $products;
    }

    public static function createProduct($data)
    {

        $idProduct = DB::table('products')->insertGetId([
            'name' => $data['name'],
            'points' => $data['points']
        ]);

        return $idProduct;
    }

    public static function deleteProduct($id)
    {
        $delete = DB::table('products')->where('id', '=', $id['id'])->delete();
    }
}
