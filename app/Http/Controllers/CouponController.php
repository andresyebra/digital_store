<?php

namespace App\Http\Controllers;

use App\Models\Coupons;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class CouponController extends Controller
{

    public function index()
    {
        $coupons = Coupons::getCoupons();

        return view('coupons.index', [
            'coupons' => $coupons
        ]);
    }

    public function create()
    {

        $data = [
            'id' => Input::get('id_coupon'),
            'name' => Input::get('name_coupon'),
            'points' => Input::get('points_coupon')];

        if (Input::get('add_coupon')) {

            $valid = Validator::make($data, [
                'name' => 'required|string|max:6',
                'points' => 'required|integer']);

            if ($valid->fails()) {
                return redirect('product/index')
                    ->withErrors($valid)
                    ->withInput();
            }

            $create = Coupons::createCoupon($data);

            return redirect()->route('coupon', ['message' => 'Coupon created success']);

        } elseif (Input::get('delete_coupon')) {

            $valid = Validator::make($data, [
                'id' => 'required|integer']);

            if ($valid->fails()) {
                return redirect('product/index')
                    ->withErrors($valid)
                    ->withInput();
            }
            $delete = coupons::deleteCoupon($data);

            return redirect()->route('coupon', ['message' => 'Coupon deleted']);
        }
    }


}
