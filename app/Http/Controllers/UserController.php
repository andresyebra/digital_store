<?php

namespace App\Http\Controllers;

use App\Models\Users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;


class UserController extends Controller
{

    public function index()
    {

        $users = Users::getUsers();

        return view('users.index', [
            'users' => $users
        ]);

    }

    public function profile($id)
    {

        $user_profile = Users::profileUser($id);
        $user_products = Users::productUser($id);
        $user_coupons = Users::couponUser($id);

        if ($user_profile == null) {

            return view('errors.404');
        }

        $total_products = 0;
        foreach ($user_products as $key => $value) {

            $total_products = $total_products + $value->points;
        }

        $total_coupons = 0;
        foreach ($user_coupons as $key => $value) {

            $total_coupons = $total_coupons + $value->points;
        }

        $aviable_points = $total_coupons - $total_products;

        return view('users.profile', [
            'profile' => $user_profile,
            'products' => $user_products,
            'coupons' => $user_coupons,
            'total_products' => $total_products,
            'total_coupons' => $total_coupons,
            'aviable_points' => $aviable_points
        ]);
    }

}
