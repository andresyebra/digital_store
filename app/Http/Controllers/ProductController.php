<?php

namespace App\Http\Controllers;

use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{

    public function __construct()
    {
        $products = Products::getProducts();
        return view('products.index', [
            'products' => $products
        ]);;
    }

    public function index()
    {
        $products = Products::getProducts();

        return view('products.index', [
            'products' => $products
        ]);
    }

    public function create()
    {

        $data = [
            'id' => Input::get('id_product'),
            'name' => Input::get('name_product'),
            'points' => Input::get('points_product')
        ];


        if (Input::get('add_product')) {

            $valid = Validator::make($data, [
                'name' => 'required|string|max:8',
                'points' => 'required|integer']);

            if ($valid->fails()) {
                return redirect('product/index')
                    ->withErrors($valid)
                    ->withInput();
            }
            $create = Products::createProduct($data);

            return redirect()->route('product', ['message' => 'Product created success']);

        } elseif (Input::get('delete_product')) {

            $valid = Validator::make($data, [
                'id' => 'required|integer']);

            if ($valid->fails()) {
                return redirect('product/index')
                    ->withErrors($valid)
                    ->withInput();
            }
            $delete = Products::deleteProduct($data);

            return redirect()->route('product', ['message' => 'Product deleted']);
        }
    }

}
